
@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-center d-flex">
	                Edit Listing
	                <span class="ml-auto">
	                	<a href="/dashboard" class="btn btn-outline-info btn-xs">
                        Back to home</a>
	                </span>
                </div>

                <div class="card-body">
                    {!! Form::open(array('url' => ['listings',$listing->id],'method' => 'POST')) !!}
                       {{Form::bsText("name",$listing->name,['placeholder'=>'Name','autocomplete'=>'off']) }}
                       {{Form::bsText("email",$listing->email,['placeholder'=>'Email','autocomplete'=>'off']) }}
                       {{Form::bsText("website",$listing->website,['placeholder'=>'wibsite','autocomplete'=>'off']) }}
                       {{Form::bsText("phone",$listing->phone,['placeholder'=>'phone','autocomplete'=>'off']) }}
                       {{Form::bsText("address",$listing->address,['placeholder'=>'address','autocomplete'=>'off']) }}
                       {{Form::bsTextArea("bio",$listing->bio,['placeholder'=>'bio','autocomplete'=>'off']) }}
                       {{Form::hidden("_method",'PUT')}}
                       {{Form::bsSubmit("Submit",['class'=>'btn btn-outline-primary']) }}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection