@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-center d-flex">
                  All Listing
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <table class="table table-striped">
                        <tr>
                            <td>Company</td>
                            <td></td>
                            <td></td>
                        </tr>
                    @if(count($listings))
                       @foreach($listings as $listing)
                          <tr>
                             <td><a href="listings/{{$listing->id}}">{{$listing->name}}</a></td>
                             <td><span class="text-muted">{{$listing->user->name}}</span></td>
                             <td></td>
                          </tr>
                       @endforeach
                    @else
                      <tr>
                          <td colspan="3">No Listing Item</td>
                      </tr>
                    @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
