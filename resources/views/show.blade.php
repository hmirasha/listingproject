
@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-center d-flex">
	                 {{$listing->name}}
	                <span class="ml-auto">
	                	<a href="/" class="btn btn-outline-info btn-xs">
                        All Listings</a>
	                </span>
                </div>
                <div class="card-body">
                 <ul class="list-group">
                   <li class="list-group-item">User : {{$listing->user->name}}</li>
                   <li class="list-group-item">Email : {{$listing->email}}</li>
                   <li class="list-group-item">Address : {{$listing->address}}</li>
                   <li class="list-group-item">Website : {{$listing->website}}</li>
                   <li class="list-group-item">Phone : {{$listing->phone}}</li>
                   <li class="list-group-item text-muted">{{$listing->bio}}</li>
                 </ul>
                </div>
            </div>
        </div>
    </div>
@endsection