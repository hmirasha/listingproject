@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-center d-flex">
                  <strong>Dashboard</strong> 
                  <span class="ml-auto">
                    <a href="listings/create" class="btn btn-outline-info btn-xs">
                        Create Listing</a>
                  </span>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <table class="table table-striped">
                        <tr>
                            <td>Company</td>
                            <td></td>
                            <td></td>
                        </tr>
                    @if(count($listings))
                       @foreach($listings as $listing)
                          <tr>
                             <td>{{$listing->name}}</td>
                             <td>
                               <a href="listings/{{$listing->id}}/edit" class="btn btn-outline-primary btn-xs pull-right">Edit</a>
                             </td>
                             <td>
                               @if(!$listing->trashed())
                                {!! Form::open(array('url' => ['listings',$listing->id],'method' => 'POST')) !!}
                                     {{Form::hidden("_method",'DELETE')}}
                                     {{Form::bsSubmit("Delete",['class'=>'btn btn-outline-danger']) }}
                                {!! Form::close() !!} 
                                   @else
                                   <div class="d-flex">
                                   {!! Form::open(array('url' => ['listings',$listing->id,'restore'],'method' => 'POST')) !!}
                                       
                                       {{Form::bsSubmit("Restore",['class'=>'btn btn-outline-success']) }}
                                    {!! Form::close() !!}
                                    &nbsp;&nbsp;
                                    {!! Form::open(array('url' => ['listings',$listing->id,'forever'],'method' => 'POST')) !!}
                                       
                                       {{Form::bsSubmit("Delete For Ever",['class'=>'btn btn-outline-danger']) }}
                                    {!! Form::close() !!} 
                                    </div>
                                   @endif
                             </td>
                          </tr>
                       @endforeach
                    @else
                      <tr>
                          <td colspan="3">No Listing Item</td>
                      </tr>
                    @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
