<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Listing;
use Faker\Generator as Faker;

$factory->define(Listing::class, function (Faker $faker) {
    return [
        "name"=>$faker->name,
        "email"=>$faker->email,
        "address"=>$faker->address,
        "website"=>$faker->url,
        "phone"=>$faker->phoneNumber,
        "bio"=>rtrim($faker->sentence(rand(1,7)),"."),
    ];
});
