<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', 'ListingController@index');

Route::get('/', 'ListingController@index');

Auth::routes();

Route::get('/dashboard', 'DashboardController@index');

Auth::routes();

Route::resource('/listings', 'ListingController');
Route::post('listings/{id}/restore', 'ListingController@restore');
Route::post('listings/{id}/forever', 'ListingController@deleteForEver');