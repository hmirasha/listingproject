<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Listing;

class ListingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $listings=Listing::all();
       return view('home')->with('listings',$listings);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
        'name'=>'required',
        'email'=>'email',
        ]);
        $listing=new Listing();
        $listing->name=$request->name;
        $listing->user_id=auth()->user()->id;
        $listing->email=$request->email;
        $listing->address=$request->address;
        $listing->website=$request->website;
        $listing->phone=$request->phone;
        $listing->bio=$request->bio;
        $listing->save();
        return redirect('/dashboard')->with('success','new listing added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $listing= Listing::find($id);
        return view('show')->with('listing',$listing);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $listing= Listing::withTrashed()->find($id);
        return view('edit')->with('listing',$listing);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
           "name"=>"required",
           "email"=>"email"
        ]);
        $listing=Listing::withTrashed()->find($id);
        if(is_null($listing))return "yes";
        $listing->name=$request->name;
        $listing->email=$request->email;
        $listing->address=$request->address;
        $listing->website=$request->website;
        $listing->phone=$request->phone;
        $listing->bio=$request->bio;
        $listing->save();
        return redirect('/dashboard')->with('success','new listing Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $listing=Listing::withTrashed()->find($id);
        $listing->delete();
        return redirect('/dashboard')->with('success','One listing deleted');

    }
         public function restore($id)
         {
             $listing=Listing::withTrashed()->find($id);
             if($listing->deleted_at){
                $listing->restore();
                return redirect('/dashboard')->with('success','One listing restore');
             }else{
                return redirect('/dashboard')->with('dander','No listing restore');
             }

         }
         public function deleteForEver($id)
         {
             $listing=Listing::withTrashed()->find($id);
             if($listing->deleted_at){
                $listing->forceDelete();
                return redirect('/dashboard')->with('success','One listing delted for ever');
             }else{
                return redirect('/dashboard')->with('dander','No listing delete');
             }

         }
}
